package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Scanner;

public class fonction {
    Scanner sc = new Scanner(System.in);
    private String serveur;
    private String pseudo;
    private int port;
    Socket socket;
    private String channel;
    BufferedWriter writer;
    BufferedReader reader;
    
    /* CLIENT CHANNEL, PORT, CHANNEL */
    	public fonction(String channel, int port, String serveur) throws IOException {
        this.channel = channel;
        this.port = port;
        this.serveur = serveur;
        this.socket = new Socket(this.serveur, this.port);
        this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }
    
    /* CLIENT SERVEUR, PSEUDO, PORT */
    public fonction(String serveur, String pseudo, int port) throws IOException {
        this.serveur = serveur;
        this.pseudo = pseudo;
        this.port = port;
        this.socket = new Socket(this.serveur, this.port);
        this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }
    	
    /* CLIENT SERVEUR, PSEUDO, PORT, CHANNEL */
    public fonction(String serveur, String pseudo, int port, String channel) throws IOException {
        this.serveur= serveur;
        this.pseudo=pseudo;
        this.port=port;
        this.channel=channel;
        this.socket = new Socket(this.serveur, this.port);
        this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }
    public void setPseudo(String pseudo){
        this.pseudo=pseudo;
    }
    public void setServeur(String serveur){
        this.serveur= serveur;
    }
    public void setPort(int port){
        this.port=port;
    }
    public void setChannel(String channel){
        this.channel=channel;
    }
    public String getPseudo(){
        return this.pseudo;
    }
    public String getServeur(){
        return this.serveur;
    }
    public int getPort(){
        return this.port;
    }
    public String getChannel(){
        return this.channel;
    }
    
    /* LIRE */
    public String lire() throws IOException{
        String line = reader.readLine();
                    if (line.startsWith("PING ")) {
                System.out.println(line);
                writer.write("PONG " + line.substring(5) + "\r\n");
                writer.write("PRIVMSG " + channel + " :I got pinged!" + "\r\n");
                writer.flush();

            }
               return line;
    }
    
    /* MESSAGE */
    public void envoyer(String tchat) throws IOException{
        writer.write("PRIVMSG " + channel + " :" + tchat + "\r\n");
        writer.flush();
    }
    
    /* CONNECTION */
    public void connect() throws IOException{
        
        writer.write("NICK " + this.pseudo + "\r\n");
        writer.write("USER " + this.pseudo + " 8 * : JavaIRC\r\n");
        writer.flush();
                String line = null;
        while ((line = reader.readLine()) != null) {
            if (line.indexOf("004") >= 0) {
                // We are now logged in.
                break;
            } else if (line.indexOf("433") >= 0) {
                System.out.println("le pseudo est deja pris.");
                return;
            }
        }
    }
    
    /* CHANGER-CHANNEL */
    public void changerChannel() throws IOException{
        writer.write("JOIN " + this.channel + "\r\n");
        writer.flush();
    }
}