package application;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;package application;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {
	
	 private fonction client ;

   public static void main(String[] args) {
       launch(args);
   }

	@Override
    public void start(Stage primaryStage) {
		
        
		
        //-----------------------------------------------------------------------------
        //----------------------- VARIABLES DECLARATION -------------------------------
        //-----------------------------------------------------------------------------
		
		
		
    	long delay = 0;
    	long intevalPeriod = 1 * 100;
        Timer timer = new Timer();
    	
        
        
        //-----------------------------------------------------------------------------
        //--------------------------------------- TITRE -------------------------------
        //-----------------------------------------------------------------------------
		
        
        
		Text scenetitle2 = new Text("Bienvenue ...");
		final Text actiontarget = new Text();
		
		
		
        //-----------------------------------------------------------------------------
        //------------------------ VARIABLE MEP SCENE UN ------------------------------
        //-----------------------------------------------------------------------------
		
		
		
        primaryStage.setTitle("Connection");
		
		Label chanel = new Label("Chanel :");
		Label nickname = new Label("votre nickname :");
		Label ip = new Label("ip du serveur :");
		Label port = new Label("port du serveur :");
		
		TextField txtchanel = new TextField();
		TextField txtnickname = new TextField();
		TextField txtip = new TextField();
		TextField txtport = new TextField();
		TextField messages = new TextField();
		TextField connec = new TextField();
		
        txtchanel.setText("#help");
        txtnickname.setText("moi");
        txtip.setText("traydent.info");
        txtport.setText("6667");
        //messages.setText("Ecrivez un message ...");
        
        Button btn1 = new Button("valider");
        Button btn2 = new Button("annuler");
		
        
        
        //-----------------------------------------------------------------------------
        //------------------------------ MON MENU -------------------------------------
        //-----------------------------------------------------------------------------
		
        
        
		MenuBar menuBar = new MenuBar();
	
		final Menu menu1 = new Menu("Fichier");
    	final Menu menu2 = new Menu("Options");
    	final Menu menu3 = new Menu("Help");
    	
    	MenuItem exitMenuItem = new MenuItem("Exit");
    	MenuItem doc1 = new MenuItem("qui sommes-nous ? ");
    	MenuItem doc2 = new MenuItem("comment utiliser ce chat ?");


    	
        //-----------------------------------------------------------------------------
        //------------------------ VARIABLE MEP SCENE DEUX  ---------------------------
        //-----------------------------------------------------------------------------
		
    	
		//---------------------ancres--------------------
		AnchorPane ancre = new AnchorPane();
		AnchorPane ancre2 = new AnchorPane();
    	
		//---------------------scene--------------------
		Scene secondScene = new Scene(ancre, 908, 515);
		Scene scene = new Scene(ancre2, 420, 320);
		
    	//---------------------stage--------------------
		Stage secondaryStage = new Stage();
        secondaryStage.setTitle("Second Stage");
        secondaryStage.setScene(secondScene);
		
		//---------------------boutton--------------------
        Button btn3 = new Button("envoyer");

        //---------------------Textarea--------------------
		TextArea chat = new TextArea();
		

		
        //-----------------------------------------------------------------------------
        //----------------------------- PARENTS/ENFANTS -------------------------------
        //-----------------------------------------------------------------------------

        

        menuBar.getMenus().addAll(menu1, menu2, menu3);
    	menu1.getItems().add(exitMenuItem);
    	menu2.getItems().add(doc1);
    	menu3.getItems().add(doc2);
    	menuBar.prefWidthProperty().bind(secondaryStage.widthProperty());
    	
        ancre.getChildren().addAll(btn3,chat,messages,connec,menuBar,scenetitle2);
        ancre2.getChildren().addAll(btn2,btn1,chanel,port,ip,nickname,txtnickname,txtport,txtip,txtchanel);
 

        
        //-----------------------------------------------------------------------------
        //--------------------------- TAILLE ET POSITION ------------------------------
        //-----------------------------------------------------------------------------
        
        
        
        //---------------------SCENE DEUX--------------------
        //--------------------------------------------
        
        
        //---------------------title--------------------
    	AnchorPane.setTopAnchor(scenetitle2, 65.0);
        AnchorPane.setRightAnchor(scenetitle2, 760.0);
        
        //---------------------textfield--------------------
    	AnchorPane.setTopAnchor(chat, 120.0);
        AnchorPane.setRightAnchor(chat, 215.0);
        
		AnchorPane.setTopAnchor(messages, 480.0);
        AnchorPane.setRightAnchor(messages, 195.0);
        
		AnchorPane.setTopAnchor(connec, 28.5);
        AnchorPane.setRightAnchor(connec, 10.0);
        
        //---------------------boutons--------------------       
        AnchorPane.setTopAnchor(btn3, 480.0);
        AnchorPane.setRightAnchor(btn3, 40.0);
        
        
        
        
      //---------------------SCENE UN--------------------
        //--------------------------------------------
        
        
       //---------------------textfield--------------------
        AnchorPane.setTopAnchor(txtchanel,180.0);
        AnchorPane.setRightAnchor(txtchanel,100.0);   
        
        AnchorPane.setTopAnchor(txtip,140.0);
        AnchorPane.setRightAnchor(txtip,100.0);    
        
        AnchorPane.setTopAnchor(txtport,100.0);
        AnchorPane.setRightAnchor(txtport,100.0);    
        
        AnchorPane.setTopAnchor(txtnickname,60.0);
        AnchorPane.setRightAnchor(txtnickname,100.0); 
        
        //--------------------label-------------------------
        AnchorPane.setTopAnchor(chanel,180.0);
        AnchorPane.setRightAnchor(chanel,260.0);
        
        AnchorPane.setTopAnchor(ip,140.0);
        AnchorPane.setRightAnchor(ip,260.0); 
             
        AnchorPane.setTopAnchor(port,100.0);
        AnchorPane.setRightAnchor(port,260.0);  
                
        AnchorPane.setTopAnchor(nickname,60.0);
        AnchorPane.setRightAnchor(nickname,260.0); 
        
        
        //---------------------boutons--------------------        
        AnchorPane.setTopAnchor(btn1,230.0);
        AnchorPane.setRightAnchor(btn1,120.0);
        
        AnchorPane.setTopAnchor(btn2,230.0);
        AnchorPane.setRightAnchor(btn2,190.0);
        
        
      //---------------------affichage des fennetres--------------------
        secondaryStage.setX(primaryStage.getX() + 10);
        secondaryStage.setY(primaryStage.getY() + 80);
          
        
      //---------------------tailles--------------------
	    connec.setPrefSize(170,432);  
        btn3.setPrefSize(126,25);
        messages.setPrefSize(690,25);
        chat.setPrefSize(670,341);
        
        
        //-----------------------------------------------------------------------------
        //----------------------------------- CSS -------------------------------------
        //-----------------------------------------------------------------------------
        

        
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		secondScene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		
		
		
        //-----------------------------------------------------------------------------
        //-------------------------------- AFFICHAGE ----------------------------------
        //-----------------------------------------------------------------------------
        
		
		
        primaryStage.setScene(scene);
    	primaryStage.show();
    	
    	
    	
        //-----------------------------------------------------------------------------
        //-------------------------------- EVENEMENTS ---------------------------------
        //-----------------------------------------------------------------------------
    	
    	
    	//-----------------------PREMIER STAGE---------------------------
    	
    	  	
        primaryStage.setOnHidden(new EventHandler<WindowEvent>() {
            @Override public void handle(WindowEvent onClosing) {

            }
          });

        
    	//se connecte au serveur IRC
        btn1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            
            public void handle(ActionEvent e) {	
                
            	//cache le stage un
                primaryStage.hide();
                
            	//affiche le stage DEUX
                secondaryStage.show();          
                
            	//declaration des variables de connections
                String serveur = txtip.getText();
            	String channel = txtchanel.getText();
            	int port = Integer.parseInt(txtport.getText());
            	String pseudo = txtnickname.getText();
               	String tchat = messages.getText();
               	
            	//on essaye de se connecter au serveur IRC
                try {
            		 client = new fonction(serveur,  pseudo,  port, channel);
            		 client.connect();        		 
            		 client.changerChannel();
            		 client.lire();
            	
            	//runnable qui affiche les resultats dans le textfield
                TimerTask task = new TimerTask() {
       		      @Override
       		      public void run() {
                    	
       		    	//on essaye de récuperer des lignes
						try {
							String line = client.lire();
							String chat2 = line;
							
					    	//on split pour recuperer uniquement "pseudo/messages
							String pseudo = chat2.split(":")[1].split("!")[0];
    						String message = chat2.substring(chat2.indexOf(":",chat2.indexOf(":")+1)+1);;
    						
    				    	//on affiche les resultats
                    	    chat.appendText("\n");
                    	    chat.appendText(pseudo + ":" + message);
							chat.appendText(tchat);
							
						    } catch (IOException e) {

							e.printStackTrace();
						    						}
       		      					
       		      						}
                									};

       		timer.scheduleAtFixedRate(task, delay, intevalPeriod);
       	        
                }
                
                catch (IOException ex){
              		 Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
              		 
              	 }
       		 };
       		 
        	});


//-----------------------FIN PREMIER STAGE---------------------------



//-----------------------SECOND STAGE---------------------------


	//vide les champs
	btn2.setOnAction(new EventHandler<ActionEvent>() {
	
	
	    public void handle(ActionEvent e) {
	        txtip.clear();
	        txtnickname.clear();
	        txtport.clear();
	        txtchanel.clear();
	        actiontarget.setText("vous avez refusé");
	    									}
	    
														});
	
	
	//envoie un message
	btn3.setOnAction(new EventHandler<ActionEvent>() {
		@Override
        public void handle(ActionEvent e) {
			
		
        	String tchat = messages.getText();
	
	try {

		 client.envoyer(tchat);
	}
	catch (IOException ex){
  		 Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);	 
							}
        									}
														});
	
	//-----------------------FIN SECOND STAGE---------------------------
	
	
	//fermeture du stage DEUX si on ferme le programme
	secondaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
        public void handle(WindowEvent we) {
            System.out.println("Stage is closing");
        	secondaryStage.close();
        	System.exit(1);
        }
    });   
	
	//fermeture du stage UN si on ferme le programme
	primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
        public void handle(WindowEvent we) {
            System.out.println("Stage is closing");
        	secondaryStage.close();
        	System.exit(1);
        }
    }); 

	//fermeture du stage DEUX si on appuie sur exit au menu
    exitMenuItem.setOnAction(actionEvent -> Platform.exit());
      
  }

}


import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;



public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
    	

        primaryStage.setTitle("Connection");
    
        GridPane grid = new GridPane();
    	
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(50, 50, 50, 50));


        Text scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        
        Label chanel = new Label("Chanel :");
        grid.add(chanel, 0, 1);

        TextField txtchanel = new TextField();
        grid.add(txtchanel, 1, 1);
        
        
        Label nickname = new Label("votre nickname :");
        grid.add(nickname, 0, 1);

        TextField txtnickname = new TextField();
        grid.add(txtnickname, 1, 1);
        
        
        Label ip = new Label("ip du serveur :");
        grid.add(ip, 0, 1);

        TextField txtip = new TextField();
        grid.add(txtip, 1, 1);
        
        
        Label port = new Label("port du serveur :");
        grid.add(port, 0, 1);

        TextField txtport = new TextField();
        grid.add(txtport, 1, 1);

        VBox vbox1 = new VBox();
        vbox1.getChildren().add(chanel);
        vbox1.getChildren().add(nickname);
        vbox1.getChildren().add(ip);
        vbox1.getChildren().add(port);
        grid.add(vbox1,0,1);
        
        VBox vbox2 = new VBox();
        vbox2.getChildren().add(txtchanel);
        vbox2.getChildren().add(txtnickname);
        vbox2.getChildren().add(txtip);
        vbox2.getChildren().add(txtport);
        grid.add(vbox2,0,3);
        
        HBox hb3 = new HBox();
        hb3.getChildren().add(chanel);
        grid.add(chanel,2,0);
        hb3.getChildren().add(txtchanel);
        grid.add(txtchanel,3,0);
        
        txtchanel.setText("#help");
        
        HBox hb4 = new HBox();
        hb4.getChildren().add(nickname);
        grid.add(nickname,2,1);
        hb4.getChildren().add(txtnickname);
        grid.add(txtnickname,3,1);
        txtnickname.setText("ptisky");
        
        HBox hb5 = new HBox();
        hb5.getChildren().add(ip);
        grid.add(ip,2,2);
        hb5.getChildren().add(txtip);
        grid.add(txtip,3,2);
        txtip.setText("traydent.info");
        
        HBox hb6 = new HBox();
        hb6.getChildren().add(port);
        grid.add(port,2,3);
        hb6.getChildren().add(txtport);
        grid.add(txtport,3,3);
        txtport.setText("6667");
        
        
        VBox vb2 = new VBox();
        vb2.getChildren().add(hb3);
        vb2.getChildren().add(hb4);
        vb2.getChildren().add(hb5);
        vb2.getChildren().add(hb6);
        vb2.setAlignment(Pos.TOP_LEFT);
        
        
        Button btn1 = new Button("valider");
        Button btn2 = new Button("annuler");
        
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn1);
        hbBtn.getChildren().add(btn2);
        grid.add(hbBtn, 1, 4);

        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6);
  
        
        primaryStage.setOnHidden(new EventHandler<WindowEvent>() {
            @Override public void handle(WindowEvent onClosing) {

            }
          });

          // show both stages.
 
        btn1.setOnAction(new EventHandler<ActionEvent>() {

        	
        	
        	
            @Override
            public void handle(ActionEvent e) {
            	
            	AnchorPane ancre = new AnchorPane(); //ancre principale
            	
                Scene secondScene = new Scene(ancre, 908, 515);
                Stage secondaryStage = new Stage();
                secondaryStage.setTitle("Second Stage");
                secondaryStage.setScene(secondScene);
            	
                actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("vous avez validé");
                primaryStage.hide();
                secondaryStage.show();


				String serveur = txtip.getText();
            	String channel = txtchanel.getText();
            	int port = Integer.parseInt(txtport.getText()) ;
            	String pseudo = txtnickname.getText();
                
            	
            	Text scenetitle2 = new Text("Bienvenue ...");
            	scenetitle2.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
            	AnchorPane.setTopAnchor(scenetitle2, 65.0);
                AnchorPane.setRightAnchor(scenetitle2, 760.0);
            	
            	final Menu menu1 = new Menu("File");
            	final Menu menu2 = new Menu("Options");
            	final Menu menu3 = new Menu("Help");
            	
            	MenuBar menuBar = new MenuBar();
            	menuBar.getMenus().addAll(menu1, menu2, menu3);
        		
            	TextArea chat = new TextArea();
            	chat.setPrefSize(670,341);
            	AnchorPane.setTopAnchor(chat, 120.0);
                AnchorPane.setRightAnchor(chat, 215.0);
                
        		
        		TextField messages = new TextField();
        		messages.setPrefSize(690,25);
        		AnchorPane.setTopAnchor(messages, 480.0);
                AnchorPane.setRightAnchor(messages, 195.0);
                messages.setText("Ecrivez un message ...");
                
                TextField connec = new TextField();
                connec.setPrefSize(170,432);
        		AnchorPane.setTopAnchor(connec, 28.5);
                AnchorPane.setRightAnchor(connec, 10.0);
        	    
        	    ScrollBar s1 = new ScrollBar();
        	    s1.setOrientation(Orientation.VERTICAL);
        	    s1.setPrefSize(2,341);
            	AnchorPane.setTopAnchor(s1, 120.0);
                AnchorPane.setRightAnchor(s1, 200.0);

        	      
                
                Button btn3 = new Button("envoyer");
                btn3.setPrefSize(126,25);
                AnchorPane.setTopAnchor(btn3, 480.0);
                AnchorPane.setRightAnchor(btn3, 40.0);
                
                ancre.getChildren().addAll(btn3,chat,messages,s1,connec,menuBar,scenetitle2);
                
                //Set position of second window, related to primary window.
                secondaryStage.setX(primaryStage.getX() + 10);
                secondaryStage.setY(primaryStage.getY() + 80);


				btn3.setOnAction(new EventHandler<ActionEvent>() {
					@Override
                    public void handle(ActionEvent e) {
						
		            	String tchat = messages.getText();
            	
            	try {
            		 fonction client2 = new fonction(serveur,  pseudo,  port, channel);
            		 client2.envoyer(tchat);
            	}
            	catch (IOException ex){
              		 Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
              		 
              	 }

                    }
                });

            	
				
            	try {
             		 fonction client = new fonction(serveur,  pseudo,  port, channel);
             		 client.setChannel(channel);
             		 client.setPseudo(pseudo);
             		 client.setPort(port);
             		 client.setServeur(serveur);
             		 client.connect();        		 
             		 client.changerChannel();
             		 client.lire();
             		 
             		 
             		 
             		long t= System.currentTimeMillis();
             		long end = t+200;
             		while(System.currentTimeMillis() < end) {
             	        	{
                          	String line = client.lire();
             	        	Thread.sleep(10);
                      	    chat.appendText(line);
                      	    chat.appendText("\n");
             	        	}
             		 	}
             	        
             		 } 
            			catch (IOException ex){
                   		 Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                   		 
                   	 } catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
	 
            }

        });

        
        btn2.setOnAction(new EventHandler<ActionEvent>() {


            public void handle(ActionEvent e) {
                actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("vous avez refusé");
            }
        });

        Scene scene = new Scene(grid, 500, 275);
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
    }
}