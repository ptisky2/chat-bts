package application;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;



public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
    	

        primaryStage.setTitle("Connection");
    
        GridPane grid = new GridPane();
    	
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(50, 50, 50, 50));


        Text scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        
        Label chanel = new Label("Chanel :");
        grid.add(chanel, 0, 1);

        TextField txtchanel = new TextField();
        grid.add(txtchanel, 1, 1);
        
        
        Label nickname = new Label("votre nickname :");
        grid.add(nickname, 0, 1);

        TextField txtnickname = new TextField();
        grid.add(txtnickname, 1, 1);
        
        
        Label ip = new Label("ip du serveur :");
        grid.add(ip, 0, 1);

        TextField txtip = new TextField();
        grid.add(txtip, 1, 1);
        
        
        Label port = new Label("port du serveur :");
        grid.add(port, 0, 1);

        TextField txtport = new TextField();
        grid.add(txtport, 1, 1);

        VBox vbox1 = new VBox();
        vbox1.getChildren().add(chanel);
        vbox1.getChildren().add(nickname);
        vbox1.getChildren().add(ip);
        vbox1.getChildren().add(port);
        grid.add(vbox1,0,1);
        
        VBox vbox2 = new VBox();
        vbox2.getChildren().add(txtchanel);
        vbox2.getChildren().add(txtnickname);
        vbox2.getChildren().add(txtip);
        vbox2.getChildren().add(txtport);
        grid.add(vbox2,0,3);
        
        HBox hb3 = new HBox();
        hb3.getChildren().add(chanel);
        grid.add(chanel,2,0);
        hb3.getChildren().add(txtchanel);
        grid.add(txtchanel,3,0);
        
        txtchanel.setText("#help");
        
        HBox hb4 = new HBox();
        hb4.getChildren().add(nickname);
        grid.add(nickname,2,1);
        hb4.getChildren().add(txtnickname);
        grid.add(txtnickname,3,1);
        txtnickname.setText("ptisky");
        
        HBox hb5 = new HBox();
        hb5.getChildren().add(ip);
        grid.add(ip,2,2);
        hb5.getChildren().add(txtip);
        grid.add(txtip,3,2);
        txtip.setText("traydent.info");
        
        HBox hb6 = new HBox();
        hb6.getChildren().add(port);
        grid.add(port,2,3);
        hb6.getChildren().add(txtport);
        grid.add(txtport,3,3);
        txtport.setText("6667");
        
        
        VBox vb2 = new VBox();
        vb2.getChildren().add(hb3);
        vb2.getChildren().add(hb4);
        vb2.getChildren().add(hb5);
        vb2.getChildren().add(hb6);
        vb2.setAlignment(Pos.TOP_LEFT);
        
        
        Button btn1 = new Button("valider");
        Button btn2 = new Button("annuler");
        
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn1);
        hbBtn.getChildren().add(btn2);
        grid.add(hbBtn, 1, 4);

        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6);
  
        
        primaryStage.setOnHidden(new EventHandler<WindowEvent>() {
            @Override public void handle(WindowEvent onClosing) {

            }
          });

          // show both stages.
 
        btn1.setOnAction(new EventHandler<ActionEvent>() {

        	
        	
        	
            @Override
            public void handle(ActionEvent e) {
            	
            	AnchorPane ancre = new AnchorPane(); //ancre principale
            	
                Scene secondScene = new Scene(ancre, 908, 515);
                Stage secondaryStage = new Stage();
                secondaryStage.setTitle("Second Stage");
                secondaryStage.setScene(secondScene);
            	
                actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("vous avez validé");
                primaryStage.hide();
                secondaryStage.show();


				String serveur = txtip.getText();
            	String channel = txtchanel.getText();
            	int port = Integer.parseInt(txtport.getText()) ;
            	String pseudo = txtnickname.getText();
                
            	
            	Text scenetitle2 = new Text("Bienvenue ...");
            	scenetitle2.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
            	AnchorPane.setTopAnchor(scenetitle2, 65.0);
                AnchorPane.setRightAnchor(scenetitle2, 760.0);
            	
            	final Menu menu1 = new Menu("File");
            	final Menu menu2 = new Menu("Options");
            	final Menu menu3 = new Menu("Help");
            	
            	MenuBar menuBar = new MenuBar();
            	menuBar.getMenus().addAll(menu1, menu2, menu3);
        		
            	TextArea chat = new TextArea();
            	chat.setPrefSize(670,341);
            	AnchorPane.setTopAnchor(chat, 120.0);
                AnchorPane.setRightAnchor(chat, 215.0);
                
        		
        		TextField messages = new TextField();
        		messages.setPrefSize(690,25);
        		AnchorPane.setTopAnchor(messages, 480.0);
                AnchorPane.setRightAnchor(messages, 195.0);
                messages.setText("Ecrivez un message ...");
                
                TextField connec = new TextField();
                connec.setPrefSize(170,432);
        		AnchorPane.setTopAnchor(connec, 28.5);
                AnchorPane.setRightAnchor(connec, 10.0);
        	    
        	    ScrollBar s1 = new ScrollBar();
        	    s1.setOrientation(Orientation.VERTICAL);
        	    s1.setPrefSize(2,341);
            	AnchorPane.setTopAnchor(s1, 120.0);
                AnchorPane.setRightAnchor(s1, 200.0);

        	      
                
                Button btn3 = new Button("envoyer");
                btn3.setPrefSize(126,25);
                AnchorPane.setTopAnchor(btn3, 480.0);
                AnchorPane.setRightAnchor(btn3, 40.0);
                
                ancre.getChildren().addAll(btn3,chat,messages,s1,connec,menuBar,scenetitle2);

                
                connec.appendText("coucou");
                System.out.println(chat); 

                

 

                 
                //Set position of second window, related to primary window.
                secondaryStage.setX(primaryStage.getX() + 10);
                secondaryStage.setY(primaryStage.getY() + 80);
                
            	

				btn3.setOnAction(new EventHandler<ActionEvent>() {
					@Override
                    public void handle(ActionEvent e) {
						
		            	String tchat = messages.getText();
            	
            	try {
            		 fonction client2 = new fonction(serveur,  pseudo,  port, channel);
            		 client2.envoyer(tchat);
            	}
            	catch (IOException ex){
              		 Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
              		 
              	 }

                    }
                });

            	
            	try {
             		 fonction client = new fonction(serveur,  pseudo,  port, channel);
             		 client.setChannel(channel);
             		 client.setPseudo(pseudo);
             		 client.setPort(port);
             		 client.setServeur(serveur);
             		 client.connect();        		 
             		 client.changerChannel();
             		 client.lire();
                
                
                
             		try {
             	        while (true) {	
             	        	for(int i = 1; i <= 1; i++)
             	        	{
             	           Thread.sleep(250);
                     		String line = client.lire();
                      	    chat.appendText(line);
                      	    chat.appendText("\n");
             	        	}
             	        }
             		 } catch (InterruptedException p) {
             	       p.printStackTrace();
             	    }
                   }
              		 
                   	 catch (IOException ex){
                   		 Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                   		 
                   	 }
	 
            }

        });

        
        btn2.setOnAction(new EventHandler<ActionEvent>() {


            public void handle(ActionEvent e) {
                actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("vous avez refusé");
            }
        });

        Scene scene = new Scene(grid, 500, 275);
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
    }
}