package bts;

import java.util.Date;

public class Vendeur extends Commercial {
	final double POURCENT_VENDEUR = 1.105;
	final int BONUS_VENDEUR = 200;
	
	public Vendeur(String vnom, String vprenom, int vage, Date vdate, double vChiffreAffaire)
	{
		super(vnom, vprenom, vage, vdate, vChiffreAffaire);
	}

	@Override
	double calculerSalaire() {
		return (this.chiffreAffaire*POURCENT_VENDEUR)+BONUS_VENDEUR;
	}
	
	public String getTitre()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}

}
