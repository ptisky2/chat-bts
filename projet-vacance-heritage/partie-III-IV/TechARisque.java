package bts;

import java.util.Date;

public class TechARisque extends Technicien implements ARisque{

	public TechARisque(String teNom, String tePrenom, int teAge, Date tedate, int nbUnites )
	{
		super(teNom, tePrenom, teAge, tedate, nbUnites);
	}

	public double getPrimeRisque()
	{
		double primeRisque=330.0;
		return primeRisque;
	}
	
	@Override
	double calculerSalaire() {
		return super.calculerSalaire()+this.getPrimeRisque();
	}
	
		
}
