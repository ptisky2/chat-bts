package bts;

import java.util.Date;

public class Technicien extends Employe {
	
	final double FACTEUR_UNITE = 10;
	private int nbUnites;
	
	
	
	public Technicien(String tNom, String tPrenom, int tAge, Date date, int tnbUnites)
	{
		super(tNom, tPrenom, tAge, date);
		this.nbUnites=tnbUnites;
	}
	
	@Override
	double calculerSalaire() {
		return FACTEUR_UNITE*nbUnites;
	}
	
	
	public String getTitre()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}
	

}
