<?php
//type du document
header('Content-type: text/html; charset=UTF-8');
//test login/mdp 
$message = null;

$pseudo = filter_input(INPUT_POST, 'pseudo');
$pass = filter_input(INPUT_POST, 'pass');

if (isset($pseudo,$pass)) 
{   
    $pseudo = trim($pseudo) != '' ? $pseudo : null;
    $pass = trim($pass) != '' ? $pass : null;
   
    if(isset($pseudo,$pass)) 
    {//connexion bdd
    $hostname = "localhost";
    $database = "chat";
    $username = "root";
    $password = "";
    

    
//afficher les erreurs
    $pdo_options[PDO::ATTR_EMULATE_PREPARES] = false;
    

    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
    

    $pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";
    
    /* Connexion */
    try
    {
      $connect = new PDO('mysql:host='.$hostname.';dbname='.$database, $username, $password, $pdo_options);
    }
    catch (PDOException $e)
    {
      exit('problème de connexion à la base');
    }
           
    $requete = "SELECT count(*) FROM membres WHERE pseudo = ?";
    
    try
    {
	//preparation de la requete d'insertion
      $req_prep = $connect->prepare($requete);   

      $req_prep->execute(array(0=>$pseudo));
      
      $resultat = $req_prep->fetchColumn();
      
      if ($resultat == 0) 
      {//requete d'insertion
        $insertion = "INSERT INTO membres(pseudo,pass,date_enregistrement) VALUES(:nom, :password, NOW())";
        
        $insert_prep = $connect->prepare($insertion);
        
        $inser_exec = $insert_prep->execute(array(':nom'=>$pseudo,':password'=>$pass));
        
        if ($inser_exec === true) 
        {//on démare une session
          if (!session_id()) session_start();
          $_SESSION['login'] = $pseudo;
          //message de confirmation
          $message = 'Votre inscription est enregistrée.';
		  header('Location: index.php');
        }   
      }
      else
      {  //si le login deja utulisé
        $message = 'Ce pseudo est déjà utilisé, changez-le.';
      }
    }
    catch (PDOException $e)
    {//si il y a un probleme d'insertion
      $message = 'Problème dans la requête d\'insertion';
    }	
  }
  else 
  { //si un des champs pas remplie
    $message = 'Les champs Pseudo et Mot de passe doivent être remplis.';
  }
}
?>
<!doctype html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Formulaire d'inscription</title>

<link rel="stylesheet" href="css_co.css"/>
</head>
<body>
<div id = "inscription">
    <form action = "inscription.php" method = "post">
    <fieldset>Inscription</fieldset>
	<a href="index.php">connection</a>
    <p><label for = "pseudo">Pseudo : </label><input type = "text" name = "pseudo" id = "pseudo" /></p>
    <p><label for = "pass">Mot de passe : </label><input type = "password" name = "pass" id = "pass" /></p>
    <p><input type = "submit" value = "Envoyer" id = "valider" /></p>
    </form>
    <p id = "message"><?= $message?:'' ?></p>
</div>
</body>
</html>