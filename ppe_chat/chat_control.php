<?php
//recupere le modele
require_once("chat_modele.php");
$bdd = bdd();
//verifie le nom et message existe bien
if(isset($_GET['name']) AND isset($_GET['message']))
{
     //ajoute un message
    ajout_message($bdd,$_GET['name'],$_GET['message']);
     
}
else
{	
	//supprime le message au bout de 30 minutes celon le modele
    expire_message($bdd);
    $message = message($bdd);
    require_once("chat_vue.php");
}
 
?>