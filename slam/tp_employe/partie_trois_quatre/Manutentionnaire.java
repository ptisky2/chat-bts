package partie_trois_et_quatre;

import java.util.Date;

public class Manutentionnaire extends Employe{

	final double SALAIRE_HORAIRE = 10.9;
	private int nbHeure;
	
	/**
	 * 
	 * @param m_Nom
	 * @param m_Prenom
	 * @param m_Age
	 * @param m_date
	 * @param p_NbHeure
	 */
	public Manutentionnaire(String m_Nom, String m_Prenom, int m_Age, Date m_date, int p_NbHeure)
	{
		super(m_Nom, m_Prenom, m_Age, m_date);
		this.nbHeure=p_NbHeure;
	}
	
	/**
	 * 
	 * @return salaire
	 */
	double calculerSalaire(){
		return SALAIRE_HORAIRE*nbHeure;
	}
	
	/**
	 * 
	 * return titre
	 */
	public String getTitre(){
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}
	
	
}
