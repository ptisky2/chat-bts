package partie_trois_et_quatre;

import java.util.Date;

public class TechARisque extends Technicien implements ARisque{

	/**
	 * 
	 * @param te_Nom
	 * @param te_Prenom
	 * @param te_Age
	 * @param te_date
	 * @param nbUnites
	 */
	public TechARisque(String te_Nom, String te_Prenom, int te_Age, Date te_date, int nbUnites )
	{
		super(te_Nom, te_Prenom, te_Age, te_date, nbUnites);
	}

	
	/**
	 * 
	 * @return prime
	 */
	public double getPrimeRisque()
	{
		double primeRisque=330.0;
		return primeRisque;
	}
	
	
	@Override
	/**
	 * 
	 * return salaire
	 */
	double calculerSalaire() {
		return super.calculerSalaire()+this.getPrimeRisque();
	}
	
		
}
