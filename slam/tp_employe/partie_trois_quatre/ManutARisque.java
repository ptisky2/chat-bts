package partie_trois_et_quatre;

import java.util.Date;

public class ManutARisque extends Manutentionnaire implements ARisque{
	
	/**
	 * 
	 * @param m_Nom
	 * @param m_Prenom
	 * @param m_Age
	 * @param m_date
	 * @param p_NbHeure
	 */
	public ManutARisque(String m_Nom, String m_Prenom, int m_Age, Date m_date, int p_NbHeure)
	{
		super(m_Nom, m_Prenom, m_Age, m_date, p_NbHeure);
	}
	
	/**
	 * 
	 * @return prime
	 */
	public double getPrimeRisque()
	{
		double primeRisque=550.0;
		return primeRisque;
	}
	
	@Override
	double calculerSalaire() {
		return super.calculerSalaire()+this.getPrimeRisque();
	}
}
