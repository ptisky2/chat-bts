package partie_un_et_deux;

import java.util.Date;
/**
 * 
 * @author morgan
 *
 */

public class Manutentionnaire extends Employe{

	final double SALAIRE_HORAIRE = 10.9;
	private int nbHeure;
	
	/**
	 * 
	 * @param m_Nom
	 * @param m_Prenom
	 * @param m_Age
	 * @param m_date
	 * @param PNbHeure
	 */
	public Manutentionnaire(String m_Nom, String m_Prenom, int m_Age, Date m_date, int PNbHeure )
	{
		super(m_Nom, m_Prenom, m_Age, m_date);
		this.nbHeure=PNbHeure;
	}
	
	/**
	 * 
	 * @return Titre
	 */
	public String getTitre()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}
	
	
	/**
	 * 
	 * @return Salaire
	 */
	double calculerSalaire()
	{
		return SALAIRE_HORAIRE*nbHeure;
	}
	
	
}
