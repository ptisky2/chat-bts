package partie_un_et_deux;

import java.util.Date;
/**
 * 
 * @author morgan
 *
 */

public abstract class Commercial extends Employe{

	
	protected double chiffreAffaire;
	
	/**
	 * 
	 * @param c_nom
	 * @param c_prenom
	 * @param c_age
	 * @param c_date
	 * @param c_chiffreAffaire
	 */
	public Commercial(String c_nom, String c_prenom, int c_age, Date c_date, double c_chiffreAffaire)
	{
		super(c_nom, c_prenom, c_age, c_date);
		this.chiffreAffaire=c_chiffreAffaire;
	}
	
	/**
	 * 
	 * @return chiffreAffaire
	 */
	public double getChiffreAffaire()
	{
		return this.chiffreAffaire;
	}
	
}
