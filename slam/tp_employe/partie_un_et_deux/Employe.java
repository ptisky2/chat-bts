package partie_un_et_deux;

import java.util.Date;
/**
 * 
 * @author morgan
 *
 */

abstract public class Employe {
	
	protected String nom;
	protected String prenom;
	protected Date dateEmbauche;
	protected int age;
	
	
	/**
	 * 
	 * @param e_Nom
	 * @param e_Prenom
	 * @param e_Age
	 * @param e_dateEmbauche
	 */
	public Employe(String e_Nom, String e_Prenom, int e_Age, Date e_dateEmbauche){
		this.nom=e_Nom;
		this.prenom=e_Prenom;
		this.age=e_Age;		
	}
	
	
	/**
	 * 
	 * @return
	 */
	public Date getDateEntree(){
		return this.dateEmbauche;
	}
	
	

	abstract double calculerSalaire();	
	/**
	 * 
	 * @return Titre
	 */
	public String getTitre()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}
	
	
	/**
	 * 
	 * @return nom
	 */
	public String getNom()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp+this.nom+this.prenom;
	}
	
	
	
	
	

}
