package partie_un_et_deux;

import java.util.Date;

/**
 * 
 * @author morgan
 *
 */
public class representant extends Commercial{

	final double POURCENT_REPRESENTANT = 1.1599;
	final int BONUS_REPRESENTANT = 500;
	
	/**
	 * 
	 * @param r_nom
	 * @param r_prenom
	 * @param r_age
	 * @param r_date
	 * @param r_chiffreAffaire
	 */
	public representant(String r_nom, String r_prenom, int r_age, Date r_date, double r_chiffreAffaire)
	{
		super(r_nom, r_prenom, r_age, r_date, r_chiffreAffaire);
	}

	
	public String getTitre()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}
	
	
	/**
	 * 
	 * @return salaire
	 */
	@Override
	double calculerSalaire() {
		return (POURCENT_REPRESENTANT*this.chiffreAffaire)+BONUS_REPRESENTANT;
	}
	

	
	
}
