package partie_un_et_deux;

import java.util.Date;

/**
 * 
 * @author morgan
 *
 */
public class Technicien extends Employe {
	
	final double FACTEUR_UNITE = 10;
	private int nbUnites;
	
	
	/**
	 * 
	 * @param t_Nom
	 * @param t_Prenom
	 * @param t_Age
	 * @param d_ate
	 * @param t_nbUnites
	 */
	public Technicien(String t_Nom, String t_Prenom, int t_Age, Date date, int t_nbUnites){
		super(t_Nom, t_Prenom, t_Age, date);
		this.nbUnites=t_nbUnites;
	}
	
	
	/**
	 * @return salaire
	 */
	@Override
	double calculerSalaire() {
		return FACTEUR_UNITE*nbUnites;
	}
	
	
	/**
	 * @return Titre
	 */
	public String getTitre(){
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}
	

}
